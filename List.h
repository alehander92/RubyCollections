#ifndef LIST_H
#define LIST_H

#include <stdexcept>
#include <cstdarg>
#include <initializer_list>
#include "Enumerable.h"

using namespace std;

class IndexOutOfRangeException : public std::runtime_error {
public:
    IndexOutOfRangeException(const string& message)
        : std::runtime_error(message) { };
};

template <class T>
class List : public Enumerable<T>
{
protected:
	T* container;
	int containerSize, containerCapacity;
	void resize(int);
public:
	List(void);
	List(int);
	List(const initializer_list<T>&);
	List(const List&);
	List& operator=(const List&);
	~List(void);

	// Adding new element
	void add(const T&);
	void add(const List<T>&);
	List& operator<<(const T&);
	List& operator<<(const List<T>&);
	void insertAt(int, const T&);
	void insertAt(int, const List<T>&);

	// Accessing element
	T& operator[](int) const;

	// Removing element
	void removeAt(int);
	void remove(const T&, int, int);
	void remove(const T&, int = 0);
	template <class Predicate> void remove(Predicate, int, int);
	template <class Predicate> void remove(Predicate, int = 0);
	void removeAll(const T&, int, int);
	void removeAll(const T&, int = 0);
	template <class Predicate> void removeAll(Predicate, int, int);
	template <class Predicate> void removeAll(Predicate, int = 0);
	void removeRange(int, int);
	void removeDuplicates(void);

	// Searching element
	int indexOf(const T&, int, int) const;
	int indexOf(const T&, int = 0) const;
	template <class Predicate> int indexOf(Predicate, int, int) const;
	template <class Predicate> int indexOf(Predicate, int = 0) const;

	// List properties
	int size(void) const;
	int capacity(void) const;
	void clear(void);
	int sizeOf(void) const;
	void free(void);
	bool isEmpty(void) const;

	// Operations on the list
	void reverse(void);
	void reverse(int, int);
	void swap(List<T>&);
	List<T> clone(void);
	List<T> subList(int, int) const;

    void each(function<void(T)> f) const;
};

template <class T>
List<T>::List(void)
{
	containerSize = 0;
	containerCapacity = 4;
	container = new T[containerCapacity];
}

template <class T>
List<T>::List(int size)
{
	containerSize = size;
	containerCapacity = 2 * size;
	container = new T[containerCapacity];
}

template <class T>
List<T>::List(const initializer_list<T>& init_list)
{
	containerSize = init_list.size();
	containerCapacity = init_list.size() * 2;
	container = new T[containerCapacity];
	for (auto obj = init_list.begin(), i = 0; obj != init_list.end(); obj++, i++)
        container[i] = *obj;
}

template <class T>
List<T>::List(const List& list)
{
	containerSize = list.containerSize;
	containerCapacity = list.containerCapacity;
	container = new T[containerCapacity];
	for (int i = 0; i < containerSize; ++i)
		container[i] = list.container[i];
}

template <class T>
List<T>& List<T>::operator=(const List& list)
{
	if (this != &list)
	{
		delete container;
		List(list);
	}
	return *this;
}

template <class T>
List<T>::~List(void)
{
	delete container;
}

template <class T>
void List<T>::add(const T& element)
{
	insertAt(containerSize, element);
}

template <class T>
void List<T>::add(const List<T>& list)
{
	insertAt(containerSize, list);
}

template <class T>
List<T>& List<T>::operator<<(const T& element)
{
	add(element);
	return *this;
}

template <class T>
List<T>& List<T>::operator<<(const List<T>& list)
{
	add(list);
	return *this;
}

template <class T>
void List<T>::insertAt(int position, const T& element)
{
	if (position > containerSize || position < 0) {
		throw IndexOutOfRangeException("The index given is out of range");
	}
	if (containerSize >= containerCapacity)
		resize(containerCapacity * 2);
	for (int j = containerSize - 1; j >= position; --j)
		container[j+1] = container[j];
	container[position] = element;
	++containerSize;
}

template <class T>
void List<T>::insertAt(int position, const List<T>& list)
{
	if (containerSize+list.containerSize >= containerCapacity)
		resize((containerSize+list.containerSize) * 2);
	for (int i = containerSize - 1; i >= 0; --i)
		container[position + i + list.containerSize] = container[position + i];
	for (int i = 0; i < list.containerSize; ++i)
		container[position+i] = list[i];
	containerSize += list.containerSize;
}

template <class T>
int List<T>::size(void) const
{
	return containerSize;
}

template <class T>
int List<T>::capacity(void) const
{
	return containerCapacity;
}

template <class T>
T& List<T>::operator[](int position) const
{
	if (position >= containerSize || position < 0)
		throw IndexOutOfRangeException("wrong");
	return container[position];
}

template <class T>
void List<T>::clear(void)
{
	containerSize = 0;
}

template <class T>
int List<T>::sizeOf(void) const
{
	return sizeof(T) * containerCapacity;
}

template <class T>
int List<T>::indexOf(const T& value, int startIndex, int endIndex) const
{
	for (int i = startIndex; i < endIndex; ++i)
		if (container[i] = value)
			return i;
	return -1;
}

template <class T>
int List<T>::indexOf(const T& value, int startIndex = 0) const
{
	return indexOf(value, startIndex, containerSize);
}

template <class T>
template <class Predicate>
int List<T>::indexOf(Predicate P, int startIndex, int endIndex) const
{
	for (int i = startIndex; i < endIndex; ++i)
		if (P(container[i]))
			return i;
	return -1;
}

template <class T>
template <class Predicate>
int List<T>::indexOf(Predicate P, int startIndex = 0) const
{
	indexOf(P, startIndex, containerSize);
}

template <class T>
void List<T>::reverse(int startIndex, int endIndex)
{
	for (int i = startIndex; i < endIndex / 2; ++i)
	{
		int temporaryVariable = container[i];
		container[i] = container[containerSize - i - 1];
		container[containerSize - i - 1] = temporaryVariable;
	}
}

template <class T>
void List<T>::reverse(void)
{
	reverse(0, containerSize);
}

template <class T>
void List<T>::swap(List<T>& list)
{
	List<T>* newList = new List<T>;
	(*newList) = list;
	list = (*this);
	(*this) = (*newList);
}

template <class T>
List<T> List<T>::clone(void)
{
	List<T> cloneList;
	for (int i = 0; i < containerSize; ++i)
		cloneList << container[i];
	return cloneList;
}

template <class T>
List<T> List<T>::subList(int startIndex, int endIndex) const
{
	List<T> subList;
	for (int i = startIndex; i < endIndex; ++i)
		subList << container[i];
	return subList;
}

template <class T>
void List<T>::remove(const T& value, int startIndex, int endIndex)
{
	int index = indexOf(value);
	if (index != -1)
	{
		for (int i = index + 1; i < containerSize; ++i)
			container[i - 1] = container[i];
		containerSize--;
	}
}

template <class T>
void List<T>::remove(const T& value, int startIndex = 0)
{
	remove(value, startIndex, containerSize);
}

template <class T>
template <class Predicate>
void List<T>::remove(Predicate P, int startIndex, int endIndex)
{
	for (int i = startIndex; i < endIndex; i++)
	{
		if (P(container[i]))
		{
			removeAt(i);
			break;
		}
	}
}

template <class T>
template <class Predicate>
void List<T>::remove(Predicate p, int startIndex = 0)
{
	remove(p, startIndex, containerSize);
}

template <class T>
void List<T>::removeAll(const T& pElement, int startIndex, int endIndex)
{
	for (int i = startIndex; i < endIndex; i++)
	{
		if (container[i] == pElement)
		{
			for (int j = i+1; j < containerSize; j++)
				container[j-1] = container[j];
			containerSize--;
		}
	}
}

template <class T>
void List<T>::removeAll(const T& value, int startIndex = 0)
{
	removeAll(value, startIndex, containerSize);
}

template <class T>
template <class Predicate>
void List<T>::removeAll(Predicate P, int startIndex, int endIndex)
{
	for (int i = startIndex; i < endIndex; i++)
		if (P(container[i]))
			removeAt(i);
}

template <class T>
template <class Predicate>
void List<T>::removeAll(Predicate p, int startIndex = 0)
{
	removeAll(p, startIndex, containerSize);
}

template <class T>
void List<T>::removeRange(int startIndex, int endIndex)
{
	int difference = endIndex - startIndex;
	for (int i = startIndex; i < endIndex; ++i)
		container[i] = container[difference + i];
	containerSize -= difference;
}

template <class T>
void List<T>::removeAt(int position)
{
	removeRange(position, position + 1);
}

template <class T>
void List<T>::removeDuplicates(void)
{
	for (int i = 0; i < containerSize - 1; ++i)
		removeAll(container[i], i + 1);
}

template <class T>
void List<T>::resize(int newCapacity)
{
	containerCapacity = newCapacity;
	T * newContainer = new T[containerCapacity];
	for (int i = 0; i < containerSize; ++i)
		newContainer[i] = container[i];
	delete container;
	container = newContainer;
}

template <class T>
void List<T>::free()
{
	containerSize = 0;
	containerCapacity = 4;
	delete container;
	container = new T[containerCapacity];
}

template <class T>
bool List<T>::isEmpty(void) const
{
	return containerSize == 0;
}

template <class T>
void List<T>::each(function<void(T)> f) const
{
	for (int i = 0; i < containerSize; i++)
		f(container[i]);
}

#endif //LIST_H

#include <iostream>
#include "List.h"
#include <string>
#include <map>
#include <iomanip>


using namespace std;

int main()
{
	//List<int> l = {5, 1, 2, 3, 4, 6};
	List<int> l;
	l << 5 << 1 << 2 << 3 << 4 << 6;
	//l.Populate(5, 1, 2, 3, 4, 6);

	l.each([](int x){
		cout << x << endl;
	});

	cout << boolalpha << l.all([](int x) {
		return x > 3;
	}) << endl;

	cout << boolalpha << l.none([](int x) {
		return x > 3;
	}) << endl;

	cout << boolalpha << l.any([](int x) {
		return x > 5;
	}) << endl;

	cout << boolalpha << l.one([](int x) {
		return x > 5;
	}) << endl;

	cout << l.first() << endl;


	cout << l.reduce(10, [](int sum, int x) {
		return sum + x;
	}) << endl;

	List<int>* m = l.filter([](int x) {
		return x % 2 == 0;
	});

	List<int>* n = l.first(3);

	m->each([](int x){
		cout << x << endl;
	});

	return 0;
}
